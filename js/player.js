let player;

class Player {
    constructor(warriorName, health, mana, strength, agility, speed) {
        this.warriorName = warriorName;
        this.health = health;
        this.mana = mana;
        this.strength = strength;
        this.agility = agility;
        this.speed = speed;
    }
}
let PlayerMoves = {
    calcAttack: function() {
        // Кто первый ударит
        let getPlayerSpeed = player.speed;
        let getEnemySpeed = enemy.speed;
        // Весь код
        let playerAttak = function() {
            let calcDamage;
            if (player.mana > 0 || player.mana > player.agility) {
                calcDamage = player.strength * player.mana / 500;
            }else{
                calcDamage = player.strength * player.agility / 1000;
            }
            let randomDamage = Math.floor(Math.random() * Math.floor(16));
            let calcOutputDamage = calcDamage + randomDamage;
            // Количество ударов в зависимости от скорости
            let numberOfHits = Math.floor(Math.random() * Math.floor(player.agility / 10) / 2) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }
        let enemyAttak = function() {
            let calcDamage;
            if (enemy.mana > 0) {
                calcDamage = enemy.strength * enemy.mana / 500;
            }else{
                calcDamage = enemy.strength * enemy.agility / 1000;
            }
            let randomDamage = Math.floor(Math.random() * Math.floor(16));
            let calcOutputDamage = calcDamage + randomDamage;
            // Количество ударов в зависимости от скорости
            let numberOfHits = Math.floor(Math.random() * Math.floor(enemy.agility / 10) / 2) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }
        let getPlayerHealth = document.querySelector(".health-player");
        let getEnemyHealth = document.querySelector(".health-enemy");
        // Чекаем, кто быстрее, если игрок, то он бьёт первым
        if (getPlayerSpeed >= getEnemySpeed) {
            let playerAttakValues = playerAttak();
            let totalDamage = playerAttakValues[0] * playerAttakValues[1];
            enemy.health = enemy.health - totalDamage;
            alert("Ти вдарив на " + playerAttakValues[0] + " урону " + playerAttakValues[1] + " раз");
            //Что случится после 0 здоровья
            if (enemy.health <= 0){
                alert("Ти виграв! Обнови браузер щоб зіграти знов");
                getPlayerHealth.innerHTML = 'Health: ' + player.health;
                getEnemyHealth.innerHTML = 'Health: 0';
            } else {
                getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                let enemyAttakValues = enemyAttak();
                let totalDamage = enemyAttakValues[0] * enemyAttakValues[1];
                player.health = player.health - totalDamage;
                alert("Він вдарив на " + enemyAttakValues[0] + " урону " + enemyAttakValues[1] + " раз");
                if (player.health <= 0){
                    alert("Він виграв! Обнови браузер щоб зіграти знов");
                    getPlayerHealth.innerHTML = 'Health: 0';
                    getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                } else {
                    getPlayerHealth.innerHTML = 'Health: ' + player.health;
                }
            }
        } else if (getEnemySpeed >= getPlayerSpeed) {
            let enemyAttakValues = enemyAttak();
            let totalDamage = enemyAttakValues[0] * enemyAttakValues[1];
            player.health = player.health - totalDamage;
            alert("Він вдарив на " + enemyAttakValues[0] + " урону " + enemyAttakValues[1] + " раз");
            //Что случится после 0 здоровья
            if (player.health <= 0){
                alert("Він виграв! Обнови браузер щоб зіграти знов");
                getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                getPlayerHealth.innerHTML = 'Health: 0';
            } else {
                getPlayerHealth.innerHTML = 'Health: ' + player.health;
                let playerAttakValues = playerAttak();
                let totalDamage = playerAttakValues[0] * playerAttakValues[1];
                enemy.health = enemy.health - totalDamage;
                alert("Ти вдарив на " + playerAttakValues[0] + " урону " + playerAttakValues[1] + " раз");
                if (enemy.health <= 0){
                    alert("Ти виграв! Обнови браузер щоб зіграти знов");
                    getEnemyHealth.innerHTML = 'Health: 0';
                    getPlayerHealth.innerHTML = 'Health: ' + player.health;
                } else {
                    getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                }
            }
        }
    }
}
