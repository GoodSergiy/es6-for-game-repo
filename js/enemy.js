let enemy;

class Enemy {
    constructor(enemyName, health, mana, strength, agility, speed) {
        this.enemyName = enemyName;
        this.health = health;
        this.mana = mana;
        this.strength = strength;
        this.agility = agility;
        this.speed = speed;
    }
}
