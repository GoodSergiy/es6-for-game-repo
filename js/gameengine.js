let Game = {
    startGame: function(warriorName){
        this.resetPlayer(warriorName);
        this.preFight();
    },
    resetPlayer: function(warriorName){
        //                               health, mana, strength, agility, speed
        switch (warriorName){
            case "Ryu":
            player = new Player(warriorName, 1200, 100, 200, 200, 400);
            break;
            case "Dhalsim":
            player = new Player(warriorName, 1500, 1000, 50, 50, 200);
            break;
            case "Guile":
            player = new Player(warriorName, 1200, 50, 600, 150, 100);
            break;
            case "Zangief":
            player = new Player(warriorName, 2000, 0, 2000, 50, 50);
            break;
            case "Ken":
            player = new Player(warriorName, 600, 100, 300, 200, 300);
            break;
            case "Bison":
            player = new Player(warriorName, 1300, 0, 600, 100, 100);
            break;
        }
        let getFighter = document.querySelector(".interface");
        getFighter.innerHTML = '<div class = "test"><div class = "interface"><img src="resources/' + warriorName.toLowerCase() + '.jpg" class="img-avatar"><div class="stat"><h3>' + warriorName + '</h3><p class="health-player">Health: ' + player.health + '</p><p>Mana: ' + player.mana + '</p><p>Strength: ' + player.strength + '</p><p>Agility: ' + player.agility + '</p><p>Speed: ' + player.speed + '</p></div></div></div>' ;
    },
    preFight: function(){
        let getHeader = document.querySelector(".header");
        let getAction = document.querySelector(".actions");
        let getArena = document.querySelector(".arena");
        getHeader.innerHTML = '<h2>Хто стане проти тебе?</h2>';
        getAction.innerHTML = '<a href="#" class="btn-prefight" onclick="Game.setFight()">Покликати бійця</a>';
        getArena.style.visibility = "visible";
    },
    setFight: function() {
        let getHeader = document.querySelector(".header");
        let getAction = document.querySelector(".actions");
        let getEnemy = document.querySelector(".arena");
        // Враги                   health, mana, strength, agility, speed
        let enemy00 = new Enemy("Ryu", 1000, 100, 200, 400, 300);
        let enemy01 = new Enemy("Dhalsim", 1500, 1000, 50, 50, 200);
        let enemy02 = new Enemy("Guile", 1200, 50, 600, 150, 100);
        let enemy03 = new Enemy("Zangief", 2000, 0, 2000, 50, 50);
        let enemy04 = new Enemy("Ken", 600, 100, 300, 300, 300);
        let enemy05 = new Enemy("Bison", 1300, 0, 600, 100, 100);
        //Случайный выбор
        let randomEnemy = Math.floor(Math.random() * Math.floor(6));
        console.log(randomEnemy);
        switch (randomEnemy){
            case 0:
            enemy = enemy00;
            break;
            case 1:
            enemy = enemy01;
            break;
            case 2:
            enemy = enemy02;
            break;
            case 3:
            enemy = enemy03;
            break;
            case 4:
            enemy = enemy04;
            break;
            case 5:
            enemy = enemy05;
            break;
        }
        getHeader.innerHTML = '<h2>Роби вже свій удар</h2>';
        getAction.innerHTML = '<a href="#" class="btn-prefight" onclick="PlayerMoves.calcAttack()">Атакувати</a>';
        getEnemy.innerHTML = '<div class="test2"><div class="interface"><img src="resources/' + enemy.enemyName.toLowerCase() + '.jpg" class="img-avatar"><div class="stat"><h3>' + enemy.enemyName + '</h3><p class="health-enemy">Health: ' + enemy.health + '</p><p>Mana: ' + enemy.mana + '</p><p>Strength: ' + enemy.strength + '</p><p>Agility: ' + enemy.agility + '</p><p>Speed: ' + enemy.speed + '</p></div></div></div>';
    }
}